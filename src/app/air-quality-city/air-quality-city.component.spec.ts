import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirQualityCityComponent } from './air-quality-city.component';

describe('AirQualityCityComponent', () => {
  let component: AirQualityCityComponent;
  let fixture: ComponentFixture<AirQualityCityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirQualityCityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AirQualityCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
