import { Component, OnInit, Input } from '@angular/core';
import { AfficheServiceService} from '../services/affiche/affiche-service.service'
import { PollutionIndex } from '../models/pollution-index.models';
import { TokenStorageService } from '../services/token-storage/token-storage.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pollution-index',
  templateUrl: './pollution-index.component.html',
  styleUrls: ['./pollution-index.component.scss']
})
export class PollutionIndexComponent implements OnInit {
  @Input() pollutionIndex!: PollutionIndex;
  defaultCity = "";
  form: any = {
    city: null
  };
  
  content?: string;
  isLoggedIn = false;
  role: string = '';
  username: string = '';



  constructor(private _afficheService : AfficheServiceService, private tokenStorage : TokenStorageService, private route: ActivatedRoute) { }
  
  pollutionIndexesCity: PollutionIndex[] | undefined;
  countResult: number;
  pollutionAverageToday : number;
  
  getAirQualityByCity$(city:string){
    console.log("entree dans getAirQualityByCity");
    this._afficheService.getAirQualityByCity$(city)
    .subscribe({
      next: (res : PollutionIndex[])=>{  this.pollutionIndexesCity = res;
       this.countResult = this.pollutionIndexesCity.length
        this.getDailyAveragePollutionByCity(this.pollutionIndexesCity);     
      },
      error: (err) => { console.log("error:"+err)} });

}

// moyenne de la pollution du jour pour la ville
public getDailyAveragePollutionByCity(tab:PollutionIndex[]):void{ 
var nbPollution: number = 0;
var nbTotal: number = 0;
  for (var i in tab) {
    nbPollution =  nbPollution +  tab[i].pollutionIndex;
    nbTotal++;
  }
  this.pollutionAverageToday = nbPollution/nbTotal;
}
onSubmit(): void { 
  const { city } = this.form;
  this.getAirQualityByCity$(city);
}

ngOnInit(): void {
  const city = this.route.snapshot.params['city'];
  this.getAirQualityByCity$(city);
  }

}