import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollutionIndexComponent } from './pollution-index.component';

describe('PollutionIndexComponent', () => {
  let component: PollutionIndexComponent;
  let fixture: ComponentFixture<PollutionIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollutionIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollutionIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
