export class PollutionIndex {

    cityName!: string;
    pollutionIndex!: number;
    day!: Date;
    cityGeoLatitude!: number;
    cityGeoLongitude!: number;
    regionNumber!:Number;
    regionName:string;
    co:number;
    so2!:number;
    no2!:number;
    o3:number;
    t:number;
    h:number;
    w:number;
    p:number; 
    count:number;

    pollutionIndexLastUpdate!: number;
  }