import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { RegionChartModel } from '../data-charts/regionChartModel';
import { RegionLastPollutionUpdateDTO } from '../dto/regionLastPollutionUpdateDTO';
import { AfficheServiceService } from '../services/affiche/affiche-service.service';
import { TokenStorageService } from '../services/token-storage/token-storage.service';

import { ChartDataset, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-bar-charts-region',
  templateUrl: './bar-charts-region.component.html',
  styleUrls: ['./bar-charts-region.component.scss']
})
export class BarChartsRegionComponent implements OnInit {
  content?: string;
  isLoggedIn = false;
  role: string = '';
  username: string = '';

  tabData:number[] = [];

  public chartData: ChartDataset[] = [
    { data: this.tabData, label: 'Dernier relevé pollution régions', backgroundColor: "rgb(16, 109, 231)", 
    hoverBackgroundColor: "#a2a3e2",
    hoverBorderColor: "rgba(255,99,132,1)", }
    
  ];
  public labels: string[ ] = [];

  @ViewChild(BaseChartDirective)
  public chart: BaseChartDirective | undefined;

  monoChartTypes: ChartType[] = ['bar', 'line'];
  monoChartType  : ChartType = 'bar'; //by default
  //monoChartTypes : ChartType[] = [  'bar' , 'pie' , 'doughnut' ,'polarArea' ];

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


  @Input() regionChartModel!: RegionChartModel;
  @Input() regionDTO!: RegionLastPollutionUpdateDTO;

  tabRegionsRaw: RegionLastPollutionUpdateDTO[];
  simpleTabRegionChartModel: RegionChartModel[];

  constructor(private _afficheService : AfficheServiceService, private tokenStorage : TokenStorageService) { }

  public getPollutionByRegionLastUpdate$(){ 
    this._afficheService.getAirQualityRegionsLastUpdate$()
   .subscribe({
     next: (res: RegionLastPollutionUpdateDTO[]) =>{this.tabRegionsRaw =  res;

      this.populateRegionChartModel(this.tabRegionsRaw);
      this.chart.update();
     },
     error: (err) => {console.log("error:"+err)} });
 }

 public populateRegionChartModel(tab:RegionLastPollutionUpdateDTO[]):void {
  for(var i=0; i<tab.length; i++){
    console.log("index i "+ i);
    switch (tab[i]._id["regionNumber"]) {
      case 11:
        this.labels[i] = "Ile-de-France";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 32:
        this.labels[i] = "Hauts-de-France"; 
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 28:
        this.labels[i] =  "Normandie";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 53:
     this.labels[i] ="Bretagne";
      this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 52:
       this.labels[i] =   "Pays-de-la-Loire";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 24:
      case 'tours':
       this.labels[i] = "Centre-val-de-Loire";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 75:
      case 'limoges':
        this.labels[i] = "Nouvelle Aquitaine";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 76:
        this.labels[i] = "Occitanie";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 93:
        this.labels[i] ="Paca";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 84:
        this.labels[i] ="Auvergne-Rh-Alpes";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 27:
      case 'besancon':
       this.labels[i] ="Bourgogne-Fr-Comte";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 44:
        this.labels[i] ="Grand Est";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      case 94:
        this.labels[i] = "Corse";
        this.tabData[i]  =   tab[i]["totalPollution"];
        break;
      default:
        console.log('');
        }
      }
 }

 ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorage.getToken();
  if (this.tokenStorage.getToken()) {
    this.isLoggedIn = true;
    this.role = this.tokenStorage.getUser().role;
    this.username = this.tokenStorage.getUser().username;
  }
    this.getPollutionByRegionLastUpdate$();
 }

 

}
