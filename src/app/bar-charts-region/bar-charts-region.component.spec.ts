import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartsRegionComponent } from './bar-charts-region.component';

describe('BarChartsRegionComponent', () => {
  let component: BarChartsRegionComponent;
  let fixture: ComponentFixture<BarChartsRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartsRegionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartsRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
