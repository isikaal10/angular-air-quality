import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ChartDataset, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { RegionChartModel } from '../data-charts/regionChartModel';
import { RegionLastPollutionUpdateDTO } from '../dto/regionLastPollutionUpdateDTO';
import { AfficheServiceService } from '../services/affiche/affiche-service.service';
import { TokenStorageService } from '../services/token-storage/token-storage.service';

@Component({
  selector: 'app-bar-charts-city',
  templateUrl: './bar-charts-city.component.html',
  styleUrls: ['./bar-charts-city.component.scss']
})
export class BarChartsCityComponent implements OnInit {
  
  content?: string;
  isLoggedIn = false;
  role: string = '';
  username: string = '';

  @Input() regionChartModel!: RegionChartModel;
  @Input() regionDTO!: RegionLastPollutionUpdateDTO;

  tabRegionsRaw: RegionLastPollutionUpdateDTO[];
  simpleTabRegionChartModel: RegionChartModel[];

  tabData = [];

  public chartData: ChartDataset[] = [
    { data: this.tabData, label: 'Dernier relevé pollution par ville', backgroundColor: "rgb(16, 109, 231)", 
    hoverBackgroundColor: "#a2a3e2",
    hoverBorderColor: "rgba(255,99,132,1)", }
    
  ];
  public labels: string[ ] = [];

  @ViewChild(BaseChartDirective)
  public chart: BaseChartDirective | undefined;

  monoChartTypes: ChartType[] = ['bar', 'line'];
  monoChartType  : ChartType = 'bar'; //by default
  //monoChartTypes : ChartType[] = [  'bar' , 'pie' , 'doughnut' ,'polarArea' ];

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


  constructor(private _afficheService : AfficheServiceService, private tokenStorage : TokenStorageService) { }

  public getPollutionByCityLastUpdate$(){ 
    this._afficheService.getAirQualityCitiesLastUpdate$()
   .subscribe({
     next: (res: RegionLastPollutionUpdateDTO[]) =>{this.tabRegionsRaw =  res;

      this.populateCityChartModel(this.tabRegionsRaw);
     },
     error: (err) => {console.log("error:"+err)} });
 }

 public populateCityChartModel(tab:RegionLastPollutionUpdateDTO[]):void {
  for(var i=0; i<tab.length; i++){
    for(var i=0; i<tab.length; i++){
      //this.tabTest[i] =  {"name": "Ile-de-France", "value": + tab[i]["totalPollution"]};
      this.tabData[i] = tab[i]["pollutionIndexLastUpdate"];
      this.labels[i] = tab[i]["cityName"];
      this.chart.update();
    }
      

      //this.labels[i] = "Ile-de-France"; this.tabData[i]  =   tab[i]["totalPollution"];
    }
  
  }


 ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorage.getToken();
  if (this.tokenStorage.getToken()) {
    this.isLoggedIn = true;
    this.role = this.tokenStorage.getUser().role;
    this.username = this.tokenStorage.getUser().username;
  }
    this.getPollutionByCityLastUpdate$();
 }

}

