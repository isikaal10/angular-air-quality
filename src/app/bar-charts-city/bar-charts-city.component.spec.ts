import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartsCityComponent } from './bar-charts-city.component';

describe('BarChartsCityComponent', () => {
  let component: BarChartsCityComponent;
  let fixture: ComponentFixture<BarChartsCityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartsCityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartsCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
