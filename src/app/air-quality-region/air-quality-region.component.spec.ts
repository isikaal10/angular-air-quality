import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirQualityRegionComponent } from './air-quality-region.component';

describe('AirQualityRegionComponent', () => {
  let component: AirQualityRegionComponent;
  let fixture: ComponentFixture<AirQualityRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirQualityRegionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AirQualityRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
