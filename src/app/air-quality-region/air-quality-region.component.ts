import { Component, OnInit } from '@angular/core';
import { PollutionIndex } from '../models/pollution-index.models';
import { AfficheServiceService } from '../services/affiche/affiche-service.service';

@Component({
  selector: 'app-air-quality-region',
  templateUrl: './air-quality-region.component.html',
  styleUrls: ['./air-quality-region.component.scss']
})
export class AirQualityRegionComponent implements OnInit {

  constructor(private _afficheService : AfficheServiceService) { }
  
  pollutionIndexesCity: PollutionIndex[] | undefined;

  tab: PollutionIndex[] | undefined;

tabRegions: any[];
  
  getAirQualityByCitiesToday$(city:string){
    console.log("entree dans getAirQualityByCity");
    this._afficheService.getAirQualityAllCitiesToday$()
    .subscribe({
      next: (res : PollutionIndex[]) =>{  this.pollutionIndexesCity = res;
     
        
      },
      error: (err) => { console.log("error:"+err)} });
}

public getPollutionByRegionLastUpdate$(){ 
   this._afficheService.getAirQualityRegionsLastUpdate$()
  .subscribe({
    next: (res: []) =>{this.tabRegions =  res;
    },
    error: (err) => {console.log("error:"+err)} });
  
  
 /* switch (this.pollutionIndexesCity[i].) {
    case 'paris':
      region_Api=" Ile-de-France"
      regionNb_Api = 11; 
      break;
    case 'lille':
    case 'amiens':
      region_Api = "Hauts-de-France";
      regionNb_Api = 32;
      break;

    default:
      console.log('');
		}*/

}
  ngOnInit(): void {
   this.getPollutionByRegionLastUpdate$();
  }

}
