import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAirQualityCitiesComponent } from './list-air-quality-cities.component';

describe('ListAirQualityCitiesComponent', () => {
  let component: ListAirQualityCitiesComponent;
  let fixture: ComponentFixture<ListAirQualityCitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAirQualityCitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAirQualityCitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
