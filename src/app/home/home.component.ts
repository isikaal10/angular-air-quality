import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { TokenStorageService } from '../services/token-storage/token-storage.service';
import { UserService } from '../services/user/user.service';
import { DOCUMENT } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content?: string;
  isLoggedIn = false;
  role: string = '';
  username: string = '';
  isRefreshed = false;

  form: any = {
    city: null
  };
  
  /*name = environment.application.name;
  angular = environment.application.angular;
  bootstrap = environment.application.bootstrap;
  fontawesome = environment.application.fontawesome;*/

  features: any;
  
  constructor(private userService: UserService, private tokenStorage: TokenStorageService, private router: Router , @Inject(DOCUMENT) private document: Document,
  @Inject(PLATFORM_ID) private platformId: object,
    private meta: Meta,
    private titleService: Title) {
      this.features =
      [
        {
          type: 'VILLE',
          description: 'pollution de l\'air  ville par ville [source: relevé toutes les 3 heures]',
          image: 's-ville.jpg',
          link: '#'
        },
        {
          type: 'VILLES',
          description: 'Classement de la pollution de l\'air de 29 villes françaises [source : dernier relevé pollution du jour]',
          image: 's-palmares-fleurs.jpg',
          link: 'cityChart'
        },
        {
          type: 'REGIONS',
          description: 'Classement de la pollution de l\'air des 13 régions françaises [source : dernier relevé pollution du jour]',
          image: 's-stats.jpg',
          link: 'regionChart'
        },
        {
          type: 'PREVISIONS',
          description: 'choisir sa destination week-end en fonction de la pollution prévisionnelle pour les 4 prochains jours',
          image: 's-pollution-fumee.jpg',
          link: 'home'
        },
        /*
        {
          type: 'Reactive Form',
          description: 'A model-driven approach to handling form inputs',
          image: 'demo-reactive-forms.png',
          link: 'forms'
        },
        {
          type: 'Template Driven Forms',
          description: 'Forms are the mainstay of business applications',
          image: 'demo-template-driven-forms.png',
          link: 'forms'
        },
        {
          type: 'Modal',
          description: 'How to implement modal windows with Angular and Bootstrap',
          image: 'demo-template-driven-forms.png',
          link: 'modal'
        },
        {
          type: 'Prism',
          description: 'Use a lightweight, extensible syntax highlighter',
          image: 'demo-template-driven-forms.png',
          link: 'prism'
        },*/
      ];
     }


     
     onSubmit(): void {
       const { city } = this.form;
       this.router.navigateByUrl(`pollutionIndexComponent/${this.form.city}`);
    }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorage.getToken();
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.role = this.tokenStorage.getUser().role;
      this.username = this.tokenStorage.getUser().username;     
    }
  }
}

