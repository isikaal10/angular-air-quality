import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PollutionIndexComponent } from './pollution-index/pollution-index.component';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { AirQualityRegionComponent } from './air-quality-region/air-quality-region.component';
import { BarChartsRegionComponent } from './bar-charts-region/bar-charts-region.component';
import { BarChartsCityComponent } from './bar-charts-city/bar-charts-city.component';
import { AppComponent } from './app.component';


const routes: Routes = [
  { path: 'pollutionIndexComponent/:city', component: PollutionIndexComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'appComponent', component:AppComponent },
  { path: 'regionComponent', component: AirQualityRegionComponent},
  { path: 'regionChart', component: BarChartsRegionComponent},
  { path: 'cityChart', component: BarChartsCityComponent}
  
  
 // { path: 'ngr-conversion', component: ConversionComponent },
          
];

@NgModule({
  imports: [RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
