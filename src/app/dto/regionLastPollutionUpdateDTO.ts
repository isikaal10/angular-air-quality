export class RegionLastPollutionUpdateDTO {
    _id: any ="";
    regionNumber: number = 0;
    regionName: string = "";
    totalPollution: number = 0;
  }