import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollutionCityDetailComponent } from './pollution-city-detail.component';

describe('PollutionCityDetailComponent', () => {
  let component: PollutionCityDetailComponent;
  let fixture: ComponentFixture<PollutionCityDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollutionCityDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollutionCityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
