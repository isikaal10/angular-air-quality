import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


//const AUTH_API = 'http://localhost:8080/api/';
const AUTH_API ="https://java-api-air-quality-staging.herokuapp.com/api/";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin':  '*'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 url = "/api/auth"; 

  constructor(private http: HttpClient) { }


  login(username: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'auth/signin', {
      username,
      password
    }, httpOptions);
    console.log()
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'auth/signup', {
      //return this.http.post(this.url + '/signup', {
      username,
      email,
      password
    }, httpOptions);
  }
}
