import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PollutionIndex } from '../../models/pollution-index.models';

@Injectable({
  providedIn: 'root'
})
export class AirQualityCitiesService {

  constructor(private _http : HttpClient) { }

  public getAirQuality$() : Observable<[PollutionIndex]>{
    let url = "http://localhost:3000/api/pollutionCity" ;
    console.log( "url = " + url);
    return this._http.get<[PollutionIndex]>(url);
  }
}
