import { TestBed } from '@angular/core/testing';

import { AirQualityCitiesService } from './air-quality-cities.service';

describe('AirQualityCitiesService', () => {
  let service: AirQualityCitiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AirQualityCitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
