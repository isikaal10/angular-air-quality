import { Injectable } from '@angular/core';
import { catchError, filter, map, Observable, of, tap, throwError } from 'rxjs';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { PollutionIndex } from '../../models/pollution-index.models';


@Injectable({
  providedIn: 'root'
})
export class AfficheServiceService {

  constructor(private _http : HttpClient) { }

  API_URL_CITY : string = "http://localhost:3000/api/pollutionCity/api/pollutionCity/city";

  public getAirQualityByCity$(city: string) : Observable<PollutionIndex[]>{
    let url =  `https://node-api-air-quality-staging.herokuapp.com/api/pollutionCity/${city}`;
    //"http://localhost:3000/api/pollutionCity/city/"+city ;
    return this._http.get<PollutionIndex[]>(url);
   // this.p.pipe(map(projects => projects.filter(proj => proj.day !== name)))
  }


  public getAirQualityAllCitiesToday$() : Observable<PollutionIndex[]>{
    let url = "https://node-api-air-quality-staging.herokuapp.com/api/pollutionCity/citiestoday";
    //"http://localhost:3000/api/pollutionCity/citiestoday" ;
    return this._http.get<PollutionIndex[]>(url);
    

  }

  public getAirQualityRegionsLastUpdate$() : Observable<[]>{
    let url = "https://node-api-air-quality-staging.herokuapp.com/api/pollutionCity/regionstoday";
    //"http://localhost:3000/api/pollutionCity/regionstoday" ;
    return this._http.get<[]>(url);
  }

  //
  public getAirQualityCitiesLastUpdate$() : Observable<[]>{
    let url = "https://node-api-air-quality-staging.herokuapp.com/api/pollutionCity/citiesLastUpdate";
    //"http://localhost:3000/api/pollutionCity/citiesLastUpdate" ;
    return this._http.get<[]>(url);
  }    

}
