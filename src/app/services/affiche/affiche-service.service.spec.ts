import { TestBed } from '@angular/core/testing';

import { AfficheServiceService } from './affiche-service.service';

describe('AfficheServiceService', () => {
  let service: AfficheServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AfficheServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
