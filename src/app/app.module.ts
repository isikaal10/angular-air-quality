import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AfficheComponent } from './affiche/affiche.component';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PollutionIndexComponent } from './pollution-index/pollution-index.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';

import { authInterceptorProviders } from './helpers/auth.interceptor';
import { HeaderComponent } from './header/header.component';
import { ListAirQualityCitiesComponent } from './list-air-quality-cities/list-air-quality-cities.component';
import { AirQualityCityComponent } from './air-quality-city/air-quality-city.component';

import * as fr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { PollutionCityDetailComponent } from './pollution-city-detail/pollution-city-detail.component';
import { AirQualityRegionComponent } from './air-quality-region/air-quality-region.component';

import { BarChartsRegionComponent } from './bar-charts-region/bar-charts-region.component';
import { BarChartsCityComponent } from './bar-charts-city/bar-charts-city.component';
import { NgChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';




@NgModule({
  declarations: [
    AppComponent,
    AfficheComponent,
    PollutionIndexComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    HeaderComponent,
    ListAirQualityCitiesComponent,
    AirQualityCityComponent,
    PollutionCityDetailComponent,
    AirQualityRegionComponent,
    BarChartsRegionComponent,
    BarChartsCityComponent,
    FooterComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgChartsModule
  ],
  providers: [authInterceptorProviders,
  {provide: LOCALE_ID, useValue: 'fr-FR'}],
  bootstrap: [AppComponent],
  
})
export class AppModule {constructor() {
  registerLocaleData(fr.default);
} }
